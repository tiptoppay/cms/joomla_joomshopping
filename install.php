<?php 
    function my_copy_all($from, $to, $rewrite = true) {
        if (is_dir($from)) {
          @mkdir($to);
          $d = dir($from);
          while (false !== ($entry = $d->read())) {
            if ($entry == "." || $entry == "..") continue;
              my_copy_all("$from/$entry", "$to/$entry", $rewrite);
            }
            $d->close();
        }
          else {
            if (!file_exists($to) || $rewrite)
            copy($from, $to);
          }
    }


$old_dir = $_SERVER['DOCUMENT_ROOT'].'/plugins/jshoppingadmin/tiptoppay_ttp/install/pm_tiptoppay_ttp/';
$new_dir =$_SERVER['DOCUMENT_ROOT'].'/components/com_jshopping/payments/pm_tiptoppay_ttp/';
my_copy_all($old_dir,$new_dir, false); 
  
  
?> 