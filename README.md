# TipTop Pay module for Joomla JoomShopping

Модуль позволит добавить на ваш сайт оплату банковскими картами через платежный сервис [TipTop Pay](https://tiptoppay.kz). 
Для корректной работы модуля необходима регистрация в сервисе.
Порядок регистрации описан в [документации TipTop Pay](https://tiptoppay.kz/#connect).

## Возможности

* Одностостадийная система;
* Двухстостадийная система;
* Выбор языка виджета;
* Выбор дизайна виджета;
* Выбор локализации виджета оплаты;
* Информирование СMS о статусе платежа;
* Поддержка онлайн-касс;
* Отправка чеков по email;
* Отправка чеков по SMS;
* Теги способа и предмета расчета;  

## Совместимость:
JoomShopping v.4.15.0 и выше;  
Joomla v.3.8.2 и выше.

### Установка через панель управления

1. Зайдите в инсталлятор расширений `Расширения` -> `Менеджер расширений` -> `Установка` -> `/administrator/index.php?option=com_installer`  
и загрузить архив. Повторите установку, для выполнения скрипта установки.

2. Активируйте расширение TipTop Pay в  меню `Расширения` -> `Менеджер расширений` -> `Управление` -> `/administrator/index.php?option=com_installer&view=manage`.

3. Далее в настройках модуля `Компоненты` -> `JoomShopping` -> `Опции` -> `Способы оплаты`:\
`/administrator/index.php?option=com_jshopping&controller=payments`  
добавьте новый способ оплаты:
 
 **Обязательные пункты:**
* Название - **TipTop Pay**;
* Псевдоним и Имя скрипта - **pm_tiptoppay_ttp**; 
* Тип оплаты - **Расширенный**.   
 
### Настройка модуля TipTop Pay

Далее перейдите в созданную платежную систему и измените настройки.

Вкладка `Конфигурация`:

* **Public_id** - Public id сайта из личного кабинета TipTop Pay;  
* **Пароль для API** - API Secret из личного кабинета TipTop Pay;  
* **Тип схемы проведения платежей** - Выбор схемы оплата платежа;   
* **Статус оплаченного заказа** - Paid;  
* **Статус авторизованного платежа** - Confirmed;  
* **Статус возврата платежа** - Refunded;  
* **Язык виджета** - Выбор языка виджета;  
* **Дизайн виджета** - Выбор дизайна виджета из 3 возможных (classic, modern, mini); 
* **Валюта заказа** - Выбор валюты заказа;
* **Использовать функционал онлайн касс** - Включение/отключение формирования онлайн-чека при оплате.
* **ИИН/БИН организации** - ИИН/БИН вашей организации или ИП, на который зарегистрирована касса;
* **НДС для заказа** - Укажите ставку НДС товаров;
* **Выберите НДС на доставку** - Укажите ставку НДС службы доставки;
* **Система налогооблажения** - Тип системы налогообложения;
* **Место осуществления расчёта** - Адреса сайта точки продаж для печати в чеке; 
* **Способ расчета** - Выбор признака способа расчета;  
* **Предмет расчета** - Выбор признака предмета расчета;  
* **Статус доставки** - Complete.  
_Приложение позволяет формировать чеки для зачета и предоплаты. Отправка второго чека возможна только при следующих способах расчета: Предоплата, Предоплата 100%, Аванс._

### Настройка вебхуков

В [личном кабинете](https://merchant.tiptoppay.kz) TipTop Pay в настройках вашего сайта вставьте следующие URL для коректной работы модуля:

* **Check**  
`http://domain.kz/index.php?option=com_jshopping&controller=checkout&task=step7&act=check_&js_paymentclass=pm_tiptoppay_ttp&no_lang=1`
* **Pay**  
`http://domain.kz/index.php?option=com_jshopping&controller=checkout&task=step7&act=pay_&js_paymentclass=pm_tiptoppay_ttp&no_lang=1`
* **Cancel**  
`http://domain.kz/index.php?option=com_jshopping&controller=checkout&task=step7&act=cancel_&js_paymentclass=pm_tiptoppay_ttp&no_lang=1`
* **Confirm**  
`http://domain.kz/index.php?option=com_jshopping&controller=checkout&task=step7&act=confirm_&js_paymentclass=pm_tiptoppay_ttp&no_lang=1`
* **Refund**  
`http://domain.kz/index.php?option=com_jshopping&controller=checkout&task=step7&act=refund_&js_paymentclass=pm_tiptoppay_ttp&no_lang=1`

Где **domain.kz** - адрес сайта.

### Changelog

**1.0.0**
- Публикация модуля